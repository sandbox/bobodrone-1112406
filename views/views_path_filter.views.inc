<?php
// $Id: $

/*
 * @file
 * Provides a filter to views that filters out nodes that do not contain in the path given in a textfield.
 */

function views_path_filter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_path_filter') . '/views/handlers',
    ),
    'handlers' => array(
      'views_path_filter_handler_filter' => array(
        'parent' => 'views_handler_filter_in_operator',
        'file' => 'views_path_filter_handler_filter.inc',
      ),
    ),
  );
}
function views_path_filter_views_data_alter(&$data) {
  $data['node']['views_path_filter'] = array(
    'title' => t('Views path filter'),
    'help' => t('Filter out nodes that not is given in any path text field'),
    'filter' => array(
      'handler' => 'views_path_filter_handler_filter',
      'label' => t('Views path filter'),
    ),
  );
}