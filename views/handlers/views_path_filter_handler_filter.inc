<?php
// $Id: $

/*
 * @file
 * Provides a filter to views that filters out nodes that do not contain in the path given in a textfield.
 * Basicly the filter, on each query, it looks for all the nodes in the given field /content type. 
 * (it probably crasches with shared fields).
 * Then it matches it against the curent path.
 * If it 'likes' the current path the node id is stored in an array.
 * L8 the actual views filter handler query is searching the database only for theese nodes 
 * in that array with nodes that have passed the deadly filter.
 * 
 * If no path matches, I kind-a just need to do a query anyhow or the system fucks up, 
 * and then I look for IN (null)... Dunno if that is so smart really....
 * 
 * It seams to work anyway. Stay out of shared fields for now!!!!
 */


class views_path_filter_handler_filter extends views_handler_filter_in_operator {
  /**
   * Constructor implementation
   */
  function construct() {
    parent::construct();
  }

  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();
    foreach (content_fields() as $key => $field) {
      if ($field['type'] == 'text') {
        $this->value_options[$key] = $key;
      }
    }
  }

  function operators() {
    $operators = array(
      'in' => array(
        'title' => t('Path is in'),
        'short' => t('in'),
        'short_single' => t('='),
        'method' => 'op_pathfind',
        'values' => 1,
      ),
      'not in' => array(
        'title' => t('Path is not in'),
        'short' => t('not in'),
        'short_single' => t('<>'),
        'method' => 'op_pathfind',
        'values' => 1,
      ),
    );
    return $operators;
  }


  function op_pathfind() {
    if (empty($this->value)) {
      return;
    }
    $this->ensure_my_table();

    foreach ($this->value as $field) {
      $field_info = content_fields($field);
      $db_info = content_database_info($field_info);
      $nids = array();

	  // get ALL nodes from the field you chose
	  $q = 'SELECT nid, %s FROM {%s}';
	  $result = db_query($q, $db_info['columns']['value']['column'], $db_info['table']);
	  // loop thru all nodes from the query
      while($row = db_fetch_object($result)) {
		// if there is a value in that field in that node
		if ($row->$db_info['columns']['value']['column']) {
		  // do the funky muzik....  tjekk if path matches for that node...
		  $path = drupal_get_path_alias($_GET['q']);
		  $page_match = drupal_match_path($path, $row->$db_info['columns']['value']['column']);
		  if ($path != $_GET['q']) {
		    $page_match = $page_match || drupal_match_path($_GET['q'], $row->$db_info['columns']['value']['column']);
		  }
		}
		else {
		  $page_match = TRUE;
	    }
		// if page match add node id to array
        if ($page_match) {
          $nids[] = $row->nid;
        }
      }
	  // if there is any true path, else do some strange magic
	  if($nids){
		  $query = "(" . implode(",", $nids) . ")";
	  }else{
		  $query = "(null)";// Pretty unsure if this really works but.... / BoboDrone
	  }
      $this->query->add_where($this->options['group'], "$this->table_alias.nid " . $this->operator . $query);	
    }
  }

}